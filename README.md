Source code for the paper 'P. Charalampopoulos, H. Chen, P. Christen, G. Loukides, N.Pisanti, S. P. Pissis, and J. Radoszewski. Pattern Masking for Dictionary Matching: Theory and Practice' in Algorithmica 2024 (forthcoming).

COMPILATION AND EXAMPLE
---------------------- 
All our methods as well as the baselines and bruteforce that are used in experiments 
compile and run on a small example with ./compile.txt 
We used g++ version (7.3.0) and tested our implementation on GNU/Linux.


INFORMATION ABOUT THE INPUT AND OUTPUT
----------------------------

Input parameters (we refer to the parameters using the example in ./compile.txt):

    file.txt: This is the input dictionary of patterns. The first line is the number of patterns.
    runs: This is the number of runs of the experiments.
    z: This is the parameter z.
    function: This the parameters to set the  bruteforce. Use  'bf' to run with bruteforce, otherwise, run without bruteforce.

The output is displayed on the screen and includes the average runtime, the average positions to mask, and the median positions to mask among many runs. 
  
For example, when we execute './main d1.txt 10 3 bf', runs = 10, z = 3 and run with bruteforce

The output of this command is the following:

**************************** OUTPUT ************************


Average time for hypergraph construction = 0 miilliseconds



**************** Results for bruteforce, baseline and GR *****************
  
 
 ============ AVG Bruteforce ============
  
  
  Average time for Bruteforce= 0 miilliseconds
  
  
  Average W Bruteforce = 5
  
  
  Median w bruteforce = 5



  ============ AVG Baseline ============
  
  
  Average time for Baseline = 0 miilliseconds
  
  
  Average W Baseline = 5
  
  
  Median w Baseline = 5



  ============ AVG GR ============
 
 
 ----------R = 3-------------
  
  
  Average time for Greddy= 0 miilliseconds
  
  
  Average W Greedy = 5
  
  
  Median w = 5
  
  
  ----------R = 4-------------
  
  Average time for Greddy= 0 miilliseconds
  
  
  Average W Greedy = 5
  
  
  Median w = 5
  
  
  ----------R = 5-------------
  
  
  Average time for Greddy= 0 miilliseconds
  
  
  Average W Greedy = 5
  
  
  Median w = 5

