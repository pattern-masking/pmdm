#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <sys/time.h>
#include <set>
#include <cstdlib>
#include <fstream>
#include <sstream>

#include <unordered_map>
#include <unordered_set>
#include <cstring>
#include <map>
#include <limits.h>

#include <bits/stdc++.h>

#include <chrono>


namespace std{
    template<> struct hash<unordered_set<int> >
    {
        std::size_t operator()(unordered_set<int> const& s) const
        {
            std::size_t hash = 0;
            for (auto && i : s){
			hash ^= std::hash<int>()(i);
	    }
            return hash;
        }
    };
}
using namespace std;

typedef unsigned long long bigint;

void printbits(bigint n){

	bigint i;
    i = 1ULL<<(sizeof(n)*CHAR_BIT-1);
   	bigint j;
   	j = 1ULL<<(sizeof(n)*CHAR_BIT-1);
   	int x=0;

   	cout<<"< ";
   	while(j>0){
    	if(n&j){
	    	cout<<63-x<<" ";  //use this if you want positions of mismatches to start from 0
	 	}
	 	++x;
    	 j >>= 1;
   	}
	cout<<"> ";
}

bool sortbysec(const pair<int,double> &a,  const pair<int,double> &b){
    return (a.second > b.second);
}

using namespace std::chrono;

void print_mymap(unordered_map<bigint, int> &a){
	cout<<"-------------------"<<endl;
	cout<<"Print map\n";

	 for(auto &it : a){
		printbits(it.first);
        cout<<"|"<<it.second<<endl;
    }
	cout<<"-------------------"<<endl;

}

//returns 1 if the entire unordered set is found,0 otherwise
bool find_mymap(unordered_map<unordered_set<int>, int> &a, unordered_set<int>& key){
	unordered_map<unordered_set<int>, int>::const_iterator it=a.find(key);

	if(it!=a.end()){
		return true;
	}
	else
		return false;
}

//returnss 1 if the element (i.e., unordered_set) is deleted, 0 otherwise
int delete_mymap(unordered_map<unordered_set<int>, int> &a, unordered_set<int>& key){
	return a.erase(key);
}


void insert_mymap(unordered_map<bigint, int> &a, bigint key, int val){
	auto it = a.find(key);
	if( it !=a.end()){
		it->second += val;
	}
	else
		a.insert(pair<bigint, int>(key,val));

}
//we add node i into our edge x
void add_node_i_to_x(bigint &x, int i){
	x = x |((1ULL << i));
}


//removes node i from our edge x
void remove_node_i_from_x(bigint &x, int i){
	x = x & (~(1ULL << i));
}

//returns the size of edge s in hypergraph a (i.e., how many bits are 1)
//it uses a gcc command that is very fast
int size_of_edge( bigint& s){
	return __builtin_popcountll(s);
}

//deletes any positions of s contained in any key
void update_mymap(unordered_map<bigint, int> &a, bigint& s,int l, bigint & positions){
	// for each element of s
	for(int i=0; i<l; ++i){
		//if s does not contain node i continue
		if((s&(1ULL<<i))==0)
			continue;
		//iterate over the map a
		for(unordered_map<bigint, int>::iterator it2=a.begin(); it2!=a.end(); ){
			 //edge of map contains node i
			if(((it2->first)&(1ULL<<i))!=0){
				remove_node_i_from_x(positions,i);
		
				bigint x; //new edge
				x=it2->first; //new edge gets the value of old edge

				int x_val=it2->second;	//value of new edge is the value of old edge

				it2=a.erase(it2); //erase old edge

				//remove node i (i.e., set the bit of node i to 0)
				x  = x & (~(1ULL << i));

				if(x!=0ULL){  //the edge has at least 1 node after deletion
				
					//a.insert(pair<bigint,int>(x,x_val));
					insert_mymap(a,x,x_val);
				}
			}
			else		//element of s not contained in the element of map
				++it2;


		}
	}
}

//deletes any positions of s contained in any key
void update_mymap_check(unordered_map<bigint, int> &a, bigint& s, int &flag ,int l, int r, bigint & positions){
	// for each element of s
	for(int i=0; i<l; ++i){
		//if s does not contain node i continue
		if((s&(1ULL<<i))==0)
			continue;
		//iterate over the map a
		for(unordered_map<bigint, int>::iterator it2=a.begin(); it2!=a.end(); ){
			bigint tmp =it2->first;
			int edge_size = size_of_edge(tmp);
			//cout<<"it2 size ="<<edge_size<<endl;
			 //edge of map contains node i
			if(((it2->first)&(1ULL<<i))!=0){
				remove_node_i_from_x(positions,i);
				edge_size--;
				bigint x; //new edge
				x=it2->first; //new edge gets the value of old edge

				int x_val=it2->second;	//value of new edge is the value of old edge

				it2=a.erase(it2); //erase old edge

				//remove node i (i.e., set the bit of node i to 0)
				x  = x & (~(1ULL << i));

				if(x!=0ULL){  //the edge has at least 1 node after deletion
					a.insert(pair<bigint,int>(x,x_val));
				}
			}
			else		//element of s not contained in the element of map
				++it2;
			if(edge_size<=r)
				flag =1;
		}
	}
}



// returns 0 if the edge is not found, and its weight otherwise
int findWeight(unordered_map<bigint, int> &a, bigint& key){
	int value=0;
	unordered_map<bigint, int>::const_iterator it=a.find(key);
	if(it!=a.end()){
		value =it->second;
		return value;
	}
	else{
		return 0;
	}
}

void update_result(bigint& s, bigint&result_vec ,int l){
	for(int i=0; i<l; ++i){
		if((s&(1ULL<<i))!=0)
			add_node_i_to_x(result_vec,i);
	}
}

//use to count the subset weight
int countWeight(unordered_map<bigint, int> &a, bigint& key, int value){
	unordered_map<bigint, int>::const_iterator it=a.find(key);
	if(it!=a.end()){
		value+=it->second;
		return value;
	}
	else{
		return value;
	}
}

//Check this using my examples in main please. Let me know if it does not do what you want
int PowerSetWeight( unordered_map<bigint, int> &hgraph_bit, bigint x){
	 int weight=0;
	 bigint A1 = x;

	 while (A1){
	    weight=countWeight(hgraph_bit, A1, weight);
	    A1 = (A1 - 1) & x;
  	 }
     return weight;

}


void finkNmax(int N, int mx[], bigint mx_string[], bigint element, int weight){
	for(int j=0; j<N; ++j){    // for each max starting with the highest
        if( weight > mx[j] ){ // 1st one we hit only (hence the break below)
        
            for(int k=N-1; k>j; --k){// copy them downward from the highest max
                mx[k] = mx[k-1];
				mx_string[k]=mx_string[k-1];
			}
            mx[j] = weight;// the one we hit
			mx_string[j] =element;
            break;
        }
    }
}

int HeaviestThreeSection(unordered_map<bigint, int > &hgraph_bit, bigint &s, bigint& positions, int l){
	int max_case1=0;
	int max_case2=0;
	int max_case3=0;

	int curr_weight=0;

	bigint s_case1=0ULL;
	bigint s_case2=0ULL;
	bigint s_case3=0ULL;

	int N=3;
	int mx[N]={0};
	bigint mx_string[N]={0ULL};

	vector<bigint> edge_size2;
    vector<bigint> edge_size1;

	for(auto &it : hgraph_bit){
		bigint tmp =it.first;
		int weight=it.second;
		 if((size_of_edge(tmp))==3){
			curr_weight = PowerSetWeight(hgraph_bit,tmp);
			if(curr_weight>max_case1){
				max_case1 = curr_weight;
				s_case1 = tmp;
			}
		 }
		 else if(size_of_edge(tmp)==2){
				edge_size2.push_back(it.first);
		 	}
		 else if(size_of_edge(tmp)==1){
			 //Case 2 select N largest weight of size one edge
		 	finkNmax(N,mx,mx_string,it.first,weight);
		
		 	}
		 }
 	//Case 2 select N largest weight of size one edge
	for(int i=0;i<N;i++){
		max_case2+=mx[i];
		if(mx_string[i]!=0ULL)
			s_case2 =  s_case2|mx_string[i];
	}

	//Case 3 Combine edge with size 2 and size 1
	for(int i=0;i<edge_size2.size();i++){
		for(int j=0; j<l; ++j){
		//if s does not contain node i continue
			if((positions&(1ULL<<j))==0)
				continue;
			bigint comb_size2_1= edge_size2[i];
			add_node_i_to_x(comb_size2_1,j);
			curr_weight = PowerSetWeight(hgraph_bit,comb_size2_1);
			if(curr_weight>max_case3){
				max_case3 = curr_weight;
				s_case3 = comb_size2_1;
			}
		}
	}

	//find max result base on three cases
	int max = std::max({max_case1, max_case2, max_case3});
	if(max_case1==max){
		s = s_case1;
	}
	else{
		if(max_case2==max)
			s = s_case2;
		else
			s = s_case3;
	}
	if(max_case2==max &&size_of_edge(s)>size_of_edge(s_case2)){
		s = s_case2;
	}
	if(max_case3==max && size_of_edge(s)>size_of_edge(s_case3)){
		s = s_case3;
	}
	return max;
}

int HeaviestTwoSection(unordered_map<bigint, int > &hgraph_bit, bigint &s){
	//maximum result in three cases
	int max=0;
	int max_case1=0;
	int max_case2=0;

	int curr_weight=0;

	bigint s_case1=0ULL;
	bigint s_case2=0ULL;

	int N=2;
	int mx[N]={0};
	bigint mx_string[N]={0ULL};

	vector<bigint> edge_size2;
    	vector<bigint> edge_size1;

	for(auto &it : hgraph_bit){
        	bigint tmp =it.first;
		int weight=it.second;
		 //Case 1 select the  largest weight of size two edge
		 if(size_of_edge(tmp)==2){
			curr_weight = PowerSetWeight(hgraph_bit,tmp);
			if(curr_weight>max_case1){
				max_case1 = curr_weight;
				s_case1 = tmp;
			}
		 }
		 else if(size_of_edge(tmp)==1){
			 //Case 2 select N largest weight of size one edge
		 	finkNmax(N,mx,mx_string,it.first,weight);
		 }
	}

	for(int i=0;i<N;i++){
		max_case2+=mx[i];
		if(mx_string[i]!=0)
			s_case2 =  s_case2|mx_string[i];
	}

	//find max result base on two cases
	if(max_case2>max_case1){
		max = max_case2;
		s =s_case2;
	}
	else{
		max = max_case1;
		s =s_case1;
	}
	return max;
}

int HeaviestOneSection(unordered_map<bigint, int > &hgraph_bit, bigint &s){
	int max =0;
	for(auto &it : hgraph_bit){
        	bigint tmp =it.first;
		int weight=it.second;
		if(size_of_edge(tmp)==1){
			if(weight>max){
				max = weight;
				s = tmp;
			}
		 }
	}
	return max;
}


void assignNodeWeight(unordered_map<bigint, int > &hgraph_bit, double node_weight[], int l, bigint & result_vec){

	int *denorm = new int[l]{};
	int *numera = new int[l]{};
	int *counter = new int[l]{};

	int size=0;

	for(auto &it : hgraph_bit){
         	bigint tmp=it.first;
		int weight=it.second;
		for(int i=0; i<l; ++i){
			//if tmp contain node i assign weight
			if(tmp&(1ULL<<i)){
				numera[i] += weight;
				counter[i]++;
				denorm[i] +=size_of_edge(tmp);
			}
		}
	}
	int flags=0;
	for(int i=0;i<l;i++){
		flags=0;
		if(size_of_edge(result_vec)>0){
			//if tmp contain node i assign weight
			if(result_vec&(1ULL<<i))
				flags=1;
		}

		if(flags!=1&& counter[i]!=0 && denorm[i]!=0){
			node_weight[i] = ((double)counter[i]*(double)numera[i])/(double)denorm[i];
		}
		else
			node_weight[i] =-1;
	}

	delete []denorm;
    delete []numera;
    delete []counter;
}

//shortern the length of edge by select the node with largest weight
void shorternH1(unordered_map<bigint, int > &hgraph_bit, double node_weight[], bigint & result_vec, int l,int flag,int r, int & tem_l, bigint& positions){
	assignNodeWeight(hgraph_bit,node_weight, l,result_vec);

	while(flag==0){
		int select_node=-1;
		float max_node_weight=0;
		for(int i=0;i<l;i++){
			if(node_weight[i]>max_node_weight && node_weight[i]!=-1){
				select_node = i;
				max_node_weight = node_weight[i];
			}
		}
		node_weight[select_node] =-1 ;
		bigint delete_node=0ULL;
		add_node_i_to_x(delete_node, select_node);

		if(select_node!=-1 && max_node_weight!=0 ){

			update_mymap_check(hgraph_bit,delete_node,flag,l,r,positions);
			result_vec =result_vec | delete_node;
			--tem_l;
		}

	}
}

//return the weight of given subset
int subset_weight( unordered_map<bigint, int> &hgraph_bit, int data[], bigint &sub,int r){
	int weight=0;
	int i=0;
   	unsigned int pow_set_size = pow(2, r);
    int counter, j;
	// ignore empty string
    for(counter = 1; counter < pow_set_size; counter++) {
		sub = 0ULL;
    	for(j = 0; j < r; j++) {
        	if(counter & (1 << j)){
				//get the subset with size r
				add_node_i_to_x(sub, data[j]);
			}
    	}
		weight = countWeight(hgraph_bit,sub, weight);

    }
	return weight;
}

void combinationUtil_greedy(unordered_map <bigint, int > &hgraph_bit,int &max_weight, int arr[], int l, int r, int index, int i,int data[], bigint &s){
	// Current cobination is ready, print it
	if (index == r) {
		bigint sub =0ULL;
		int weight =0;
		int temp_weight= subset_weight(hgraph_bit,data,sub,r);
		if(temp_weight>=max_weight){
			max_weight = temp_weight;
			s = sub;
		}

		return;
	}
	// When no more elements are there to put in data[]
	if (i >= l)
		return;
	// current is included, put next at next location
	data[index] = arr[i];
	combinationUtil_greedy(hgraph_bit, max_weight, arr, l, r, index + 1, i + 1,data,s);
	// current is excluded, replace it with next
	// (Note that i+1 is passed, but index is not changed)
	combinationUtil_greedy(hgraph_bit,max_weight, arr, l, r, index, i + 1,data, s);
}

//Bruteforce
int bruteforce(unordered_map<bigint, int > &hgraph_bit, int l, int r , bigint &s){
	int max_weight =0;

	int arr[l]; // arr store the positions in query to count the subset
	for(int i=0;i<l;i++){
		arr[i] = i;
	}
	for(int i=1;i<=r;i++){
		int data[i];
		combinationUtil_greedy(hgraph_bit, max_weight, arr, l, i, 0, 0,data, s);
	}
	return max_weight;
}


int getMinW(unordered_map<bigint, int > &hgraph_bit, int l, int z, bigint record_s, int record_matching_patterns,bigint &result_vec, int flag, int old_z, bigint& positions){
	int matching_patterns=0;
    bigint s=0ULL, result_vec_tmp=result_vec, record_s_tmp=record_s;

	while(true){
      if(flag>4)
       	matching_patterns=bruteforce(hgraph_bit, l, flag-1,s);

      else if(flag==4)
		matching_patterns = HeaviestThreeSection(hgraph_bit,s,positions,l);

      else if(flag==3)
		matching_patterns = HeaviestTwoSection(hgraph_bit,s);

      else if(flag==2)
		matching_patterns = HeaviestOneSection(hgraph_bit,s);

	  if(matching_patterns>=z && flag >1){
 			record_s_tmp=s;
			flag=flag-1;
		}
      else
		break;
    }
        result_vec=result_vec | record_s_tmp;
  
}

void greedy_new(unordered_map<bigint, int > hgraph_bit, int z,int l,int old_z,bigint & result_vec, double node_weight[],int r, int tem_l, bigint positions){

	int flag=0;
	bigint s=0ULL;   // seleted edge
	int matching_patterns = 0;

	while(z>0){
		if(s!=0ULL){
			result_vec = result_vec|s;
			update_mymap(hgraph_bit,s,l,positions);
		
			if(hgraph_bit.size()==0){
				cout<<"Hypergraph is empty."<<endl;
				return;
			}
			s = 0ULL;
		}

		if(tem_l>3 && r>3){
			int tem_flag=0;
			if(tem_l>=r){
				matching_patterns=bruteforce(hgraph_bit, l, r ,s);
				tem_flag= r;
			}
			else {
				matching_patterns=bruteforce(hgraph_bit, l, tem_l,s);
				tem_flag= tem_l;
			}
			if(matching_patterns==0){
				shorternH1(hgraph_bit,node_weight,result_vec,l,0,tem_flag,tem_l, positions);		
			}
			else{
				z = z-matching_patterns;
				if(z>0)
					tem_l = tem_l-size_of_edge(s);
				else flag = tem_flag;
			}
		}
		else{      
			if(tem_l>=3){
				matching_patterns = HeaviestThreeSection(hgraph_bit,s,positions,l);
				if(matching_patterns==0){
					shorternH1(hgraph_bit,node_weight,result_vec,l,0,3,tem_l,positions);
				}
				else{
					z = z-matching_patterns;
					if(z>0)
						tem_l = tem_l-size_of_edge(s);
					else flag = 3;
				}
			}
			else{
				if(tem_l==2){
					matching_patterns = HeaviestTwoSection(hgraph_bit,s);
					z = z-matching_patterns;
					if(z<=0){
						flag = 2;
						break;
					}
				}
					//replace last one position with #
				else {
					if(tem_l==1){
					matching_patterns = HeaviestOneSection(hgraph_bit,s);
					z = z-matching_patterns;
						if(z<=0){
							result_vec=result_vec|s;
							return;
						}
					}
					else
						break;
				}
			}
		}
	}

	//Get the minimum W
	//record previous result
	int record_z = z;
	int record_matching_patterns = matching_patterns;
	bigint record_s=s;

	z = z+matching_patterns;

	getMinW( hgraph_bit,l,  z,  record_s,  record_matching_patterns, result_vec, flag,old_z,positions);
	return;

}

void combinationUtil(unordered_map<bigint , int > &hgraph_bit,int z, int arr[], int l, int r, int index, int i,int data[], bigint &result_bf)
{
	// Current cobination is ready, print it
	if (index == r) {
		bigint sub =0ULL;
		int weight =0;
		int temp_weight= subset_weight(hgraph_bit,data,sub,r);

		if(temp_weight>=z){
			result_bf = sub;
			return;
		}
		return;
	}
	// When no more elements are there to put in data[]
	if (i >= l)
		return;
	// current is included, put next at next location
	data[index] = arr[i];
	combinationUtil(hgraph_bit, z, arr, l, r, index + 1, i + 1,data,result_bf);

	combinationUtil(hgraph_bit,z, arr, l, r, index, i + 1,data, result_bf);
}



string ReadNthLine(const string& filename,long int N, long int d){
   ifstream in(filename.c_str());
   string s;
   s.reserve(d);
   //skip N lines
   for(long int i = 0; i < N; ++i)
       getline(in, s);

   getline(in,s);
   in.close();
   return s;
}

void ReadDataset(const string& filename, long int d, vector<string>& my_dataset){
   ifstream in(filename.c_str());
   string s;
   s.reserve(d);
   //skip 1st line
   getline(in,s);
   while(getline(in,s)){
	    my_dataset.push_back(s);
   }
   in.close();
}

string getRanStr(int len,int alphabet_size) {
   	string W;
    for (int i = 0; i < len; ++i) {
        W += (rand() % alphabet_size) + 'a';
    }
    return W;
}



int main( int argc, const char* argv[] )
{

	int runs=atoi(argv[2]);
	int old_z =  atoi(argv[3]);
	//int r = atoi(argv[4]);
	string flag_bf = argv[4];

	vector<double> avg_time;
	vector<double> avg_time4;
	vector<double> avg_time5;

	vector<double> avg_time_bf;
	vector<double> avg_time_base;

	vector<double> avg_hgraph_time;

	vector<int> avg_w;
	vector<int> avg_w4;
	vector<int> avg_w5;

	vector<int> avg_w_bf;
	vector<int> avg_w_base;
	
	long int d = 0;	 //size of dictionary
	string q;

	int temp_runs=runs;

	int no_queries_with_w0=0;

	vector<string> my_dataset;
	ReadDataset(argv[1],d,my_dataset);

	 timeval tv;
         gettimeofday(&tv, 0);

         srand((int)tv.tv_usec);

	while(runs>0){
		unordered_map<bigint, int > hgraph_bit;
		unordered_map<bigint, int > hgraph_bit4;
		unordered_map<bigint, int > hgraph_bit5;
		
		bigint positions =0ULL; //total mismatch positions
		bigint result_bf=0ULL;
		bigint result_base=0ULL;

		clock_t begin_hgraph = clock();

		FILE *dictionary;
		char t[255];
		sprintf(t, "%s", argv[1]);

		dictionary=fopen(t, "r");
		if(dictionary==0){
			printf("File not found\n");
			return 0;
		}
		fscanf(dictionary,"%ld",&d);

		char *b=(char*) calloc(d, sizeof(char));
		int z = old_z;

		//get a random query from the file
		long int rand_num;

		if(d>0)
			rand_num = rand() % d + 1;

	    q=my_dataset.at(rand_num-1);

		int l = q.size();

		auto hgraph_start = high_resolution_clock::now();

		if(old_z>d){
			cout<<endl<<"z larger than the size of the dictionary, we can not find a result."<<endl;
			return false;
		}

		for(vector<string>::const_iterator my_dataset_it=my_dataset.begin();my_dataset_it!=my_dataset.end();++my_dataset_it){
			if(z>0){
				string pattern=*my_dataset_it;
                int i;
				bigint x=0ULL;
				for(int i=0;i<l;++i){
					if(q[i]!=pattern[i]){
						add_node_i_to_x(x, i);
					}
				}
				if(x!=0ULL){
					insert_mymap(hgraph_bit,x,1);
					positions = positions| x;
				}
				else
					z=z-1;
			}
		}

		auto hgraph_end = high_resolution_clock::now();

		auto duration_hgraph = duration_cast<milliseconds>(hgraph_end - hgraph_start);
		avg_hgraph_time.push_back(duration_hgraph.count());

		hgraph_bit4 = hgraph_bit;
		hgraph_bit5 = hgraph_bit;

		if(z==0){
			cout<<endl<<"There are z ="<<old_z<<" patterns match with query, we dont need replace any position with #."<<endl;
			no_queries_with_w0++;
			
			avg_w.push_back(0);
			avg_w4.push_back(0);
			avg_w5.push_back(0);
			if(flag_bf.compare("bf") == 0)
				avg_w_bf.push_back(0);
			avg_w_base.push_back(0);
			runs--;
			continue;
		}

		if(hgraph_bit.size()==0){
			cout<<"Hypergraph is empty."<<endl;
			return false;
		}

		/***************************** START BRUTEFORCE *********************************/
		//Bruteforce
		if(flag_bf.compare("bf") == 0){

			int arr[l]; // arr store the positions in query to count the subset
			for(int i=0;i<l;i++){
				arr[i] = i;
			}
			auto bf_start = high_resolution_clock::now();
			// r is use the size of subset
			for(int r=1;r<=l;r++){
				if(size_of_edge(result_bf)==0){
					int data[r];
					combinationUtil(hgraph_bit, z, arr, l, r, 0, 0,data, result_bf);
				}
				else break;
			}
			auto bf_end = high_resolution_clock::now();
			auto duration_bf = duration_cast<milliseconds>(bf_end - bf_start);

			if(size_of_edge(result_bf)==0){
				runs--;
				cout<<"Bruteforce result cannot be 0."<<endl;
				return false;
			}
			avg_time_bf.push_back(duration_bf.count());
			avg_w_bf.push_back(size_of_edge(result_bf));

		}
		/*****************************END BRUTEFORCE*********************************/
		
		/*****************************START BASELINE*********************************/

		auto base_start = high_resolution_clock::now();
		double node_weight_base[l];
		assignNodeWeight(hgraph_bit,node_weight_base,l,result_base);
	
		int matching_patterns=0;
		while(matching_patterns<z){
			int select_node=-1;
			float max_node_weight=0;
			for(int i=0;i<l;i++){
				if(node_weight_base[i]>max_node_weight && node_weight_base[i]!=-1){
					select_node = i;
					max_node_weight = node_weight_base[i];
				}
			}
			node_weight_base[select_node] =-1 ;
			bigint delete_node=0ULL;
			add_node_i_to_x(delete_node, select_node);
			if(select_node!=-1 && max_node_weight!=0 ){
				result_base =result_base | delete_node;
			}
			matching_patterns = PowerSetWeight(hgraph_bit,result_base);	

		}
		auto base_end = high_resolution_clock::now();
		auto duration_base = duration_cast<milliseconds>(base_end - base_start);
		
		avg_time_base.push_back(duration_base.count());
		avg_w_base.push_back(size_of_edge(result_base));

		/*****************************END BASELINE*********************************/	

		int tem_l = size_of_edge(positions);
	
		/***************************** START GR *********************************/	
		for(int r=3;r<=5;r++){
			double node_weight[l];
			bigint result_vec=0ULL;
			auto greg_start = high_resolution_clock::now();

			greedy_new(hgraph_bit, z,l,old_z,result_vec, node_weight,r,tem_l,positions);	
			auto greg_end = high_resolution_clock::now();
			auto duration = duration_cast<milliseconds>(greg_end - greg_start);

			if(r==3){
				avg_time.push_back(duration.count());
				avg_w.push_back(size_of_edge(result_vec));

			}
			if(r==4){
				avg_time4.push_back(duration.count());
				avg_w4.push_back(size_of_edge(result_vec));
			}
			if(r==5){
				avg_time5.push_back(duration.count());
				avg_w5.push_back(size_of_edge(result_vec));
			}
		}
		runs --;
	}
	cout<<endl<<"**************************** OUTPUT ************************"<<endl;
	//GET average time for create hgraph in many runs
	double total_time_hgraph=0.0;

	for(int m=0;m<avg_hgraph_time.size();m++)
		total_time_hgraph+=avg_hgraph_time[m];

	cout<<"Average time for hypergraph construction = "<< (double)total_time_hgraph/(double)avg_hgraph_time.size()<<" miilliseconds"<<endl<<endl;;

	cout<<"**************** Results for bruteforce, baseline and GR *****************"<<endl;

	if(flag_bf.compare("bf") == 0){
		cout<<"  ============ AVG Bruteforce ============"<<endl;
		//GET average time for many runs
		double total_time_bf=0.0;
		for(int m=0;m<avg_time_bf.size();m++)
			total_time_bf+=avg_time_bf[m];
		cout<<"  Average time for Bruteforce= "<< (double) total_time_bf/(double)avg_time_bf.size()<<" miilliseconds"<<endl;

		//Return avg w
		double total_w_bf=0.0;
		for(int m=0;m<avg_w_bf.size();m++)
			total_w_bf+=avg_w_bf[m];
		cout<<"  Average W Bruteforce = "<< (double)total_w_bf/(double)temp_runs<<endl;

		//Return median w
		int size_bf = avg_w_bf.size();

		  if (size_bf == 0){
			cout<<"  Median w bruteforce = "<<0<<endl;
			return 0;  
		  }
		  else{
			sort(avg_w_bf.begin(), avg_w_bf.end());
			if (size_bf % 2 == 0)
			  cout<<"  Median w bruteforce = "<< (avg_w_bf[size_bf / 2 - 1] + avg_w_bf[size_bf / 2]) / 2<<endl;
			else
			  cout<<"  Median w bruteforce = "<< avg_w_bf[size_bf / 2]<<endl;
		  }
		cout<<endl;
	}
	
	cout<<"  ============ AVG Baseline ============"<<endl;
		//GET average time for many runs
		double total_time_base=0.0;
		for(int m=0;m<avg_time_base.size();m++)
			total_time_base+=avg_time_base[m];
		cout<<"  Average time for Baseline = "<< (double) total_time_base/(double)avg_time_base.size()<<" miilliseconds"<<endl;

		//Return avg w
		double total_w_base=0.0;
		for(int m=0;m<avg_w_base.size();m++)
			total_w_base+=avg_w_base[m];
		cout<<"  Average W Baseline = "<< (double)total_w_base/(double)temp_runs<<endl;

		//Return median w
		int size_bf = avg_w_base.size();

		  if (size_bf == 0){
			cout<<"  Median w Baseline = "<<0<<endl;
			return 0;  
		  }
		  else{
			sort(avg_w_base.begin(), avg_w_base.end());
			if (size_bf % 2 == 0)
			  cout<<"  Median w Baseline = "<< (avg_w_base[size_bf / 2 - 1] + avg_w_base[size_bf / 2]) / 2<<endl;
			else
			  cout<<"  Median w Baseline = "<< avg_w_base[size_bf / 2]<<endl;
		  }
		cout<<endl;

	cout<<"  ============ AVG GR ============"<<endl;

	cout<<"  ----------R = 3-------------"<<endl;
	//GET average time for many runs
	double total_time=0.0;
	for(int m=0;m<avg_time.size();m++)
		total_time+=avg_time[m];
	cout<<"  Average time for Greddy= "<< (double) total_time/(double)avg_time.size()<<" miilliseconds"<<endl;

	//Return avg w
	double total_w=0.0;
	for(int m=0;m<avg_w.size();m++)
		total_w+=avg_w[m];
	cout<<"  Average W Greedy = "<< (double)total_w/(double)temp_runs<<endl;

	//Return median w
	int size = avg_w.size();
	  if (size == 0){
		cout<<"  Median w Greedy = "<<0<<endl;
		return 0;  
	  }
	  else{
		sort(avg_w.begin(), avg_w.end());
		if (size % 2 == 0)
		  cout<<"  Median w = "<< (avg_w[size / 2 - 1] + avg_w[size / 2]) / 2<<endl;
		else
		  cout<<"  Median w = "<< avg_w[size / 2]<<endl;
	  }

	cout<<"  ----------R = 4-------------"<<endl;
	//GET average time for many runs
	total_time=0.0;
	for(int m=0;m<avg_time4.size();m++)
		total_time+=avg_time4[m];
	cout<<"  Average time for Greddy= "<< (double) total_time/(double)avg_time.size()<<" miilliseconds"<<endl;

	//Return avg w
	total_w=0.0;
	for(int m=0;m<avg_w4.size();m++)
		total_w+=avg_w4[m];
	cout<<"  Average W Greedy = "<< (double)total_w/(double)temp_runs<<endl;

	//Return median w
	 size = avg_w4.size();
	  if (size == 0){
		cout<<"  Median w Greedy = "<<0<<endl;
		return 0; 
	  }
	  else{
		sort(avg_w4.begin(), avg_w4.end());
		if (size % 2 == 0)
		  cout<<"  Median w = "<< (avg_w4[size / 2 - 1] + avg_w4[size / 2]) / 2<<endl;
		else
		  cout<<"  Median w = "<< avg_w4[size / 2]<<endl;
	  }


	cout<<"  ----------R = 5-------------"<<endl;
	//GET average time for many runs
	total_time=0.0;
	for(int m=0;m<avg_time5.size();m++)
		total_time+=avg_time5[m];
	cout<<"  Average time for Greddy= "<< (double) total_time/(double)avg_time5.size()<<" miilliseconds"<<endl;

	//Return avg w
	total_w=0.0;
	for(int m=0;m<avg_w5.size();m++)
		total_w+=avg_w5[m];
	cout<<"  Average W Greedy = "<< (double)total_w/(double)temp_runs<<endl;

	//Return median w
	 size = avg_w5.size();
	  if (size == 0){
		cout<<"  Median w Greedy = "<<0<<endl;
		return 0; 
	  }
	  else{
		sort(avg_w5.begin(), avg_w5.end());
		if (size % 2 == 0)
		  cout<<"  Median w = "<< (avg_w5[size / 2 - 1] + avg_w5[size / 2]) / 2<<endl;
		else
		  cout<<"  Median w = "<< avg_w5[size / 2]<<endl;
	  }
	
	return 1;
}
